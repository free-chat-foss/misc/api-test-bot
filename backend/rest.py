from typing import Optional
from os import getenv
from time import time
from requests import Request, Response, Session

class Test:
    '''
    This class is a thin wrapper around the requests module which really only
    serves to add some context to each underlying http call.
    The whole point of the wrapper is to provide a view layer wherein we
    can easily discern what is going on at a high level.
    '''
    def __init__(self, method: str, target: str, headesr: dict, desc:Optional[str], **kwargs):
        self.time = time()

        self.request: Request = Request(method, target, headers=headers, **kwargs)
        self.description = _set_desc(desc)
        # Init to None first as we must fire the request manually
        self.response: Optional[Response] = None

    def  _set_desc(self, desc: Optional[str]) -> str:
        if desc is not None:
            return desc
        return f'{self.request.method} {self.request.url}'

    def fire(self, session: Session):
        self.response = session.send(self.request.prepare())

    def __str__(self) -> str:
        '''
        Typical representation that is used  in pipelines to quickly
        dump out what the result of this test actually was
        '''
        headline = f'{self.id}-{self.request.method} => {self.request.url}'
        if self.response is None:
            status = '\tUnused'
        else:
            status = f'\t{self.response.status_code} - {self.response.headers}'

        if self.response is None:
            body = '\tNo body to show'
        else:
            if getenv('SHOW_FULL_RESPONSE_BODY'):
                body = f'\tFull body: {self.response.content()}'
            else:
                body = f'\tPartial Body: {self.response.content[:32]}'


        return headline + '\n' + status + '\n' + body + '\n'


