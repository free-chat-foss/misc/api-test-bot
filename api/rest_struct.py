'''
This module contains all the data structures that the
REST/JSON API may return, this is written to spec
according to what freechat.shockrah.xyz says the API
is supposed to do.
'''

from typing import Optional

class Member:
    '''
    This Member type actually represents the PUBLIC member type and thus
    does not contain data which isn't already exposed to instance members
    '''
    def __init__(self, id: int, nick: str, status: int, permissions: int):
        self.id = id
        self.nick = nick
        self.status = status
        self.permissions = permissions

class MemberAuth:
    '''
    Contains the private auth data for a member
    @WARN: jwt may be optional as this must be:
        1. Refreshed regularly
        2. Potentially invalidated by server refresh periods
        3. Invalidated due to outright expirey
    The expirey issue is more prevalent than other platforms because
    JWT's by default expire every 72 hours
    '''
    def __init__(self, secret: str, jwt: Optional[str]):
        self.secret = secret
        self.jwt = jwt

class PrivateMember:
    def __init__(self, public: Member, auth: MemberAuth):
        self.public = public
        self.auth = auth

class ChannelType:
    VOICE = 1
    TEXT = 2

class Channel:
    def __init__(self, id: int, name: str, kind: int, description: Optional[str]):
        self.id = id
        self.name = name
        self.kind = kind
        self.description = description

    def is_voice(self):
        return self.kind == ChannelType.VOICE

    def is_text(self):
        return self.kind == ChannelType.TEXT


class Invite:
    def __init__(self, id: int, expires: Optional[bool]):
        self.id = id
        self.expires = expires

class DeletedInvite:
    '''
    Resultant data from a succesful DELETE /invite/delete/<id>
    '''
    def __init__(self, id: int):
        self.id = id


class Neighbor:
    '''
    Represents a single admin created neighbor instance
    '''
    def __init__(
            self, 
            url: str,
            wsurl: Optional[str],
            name: str,
            description: str,
            tags: list[str]):

        self.url = url
        self.wsurl = wsurl
        self.name = name
        self.description = description
        self.tags = tags


