from backend.rest import Test
from requests import Session
import time
import api


class Bot:
    '''
    Here is where we keep the state for the bot itself
    this Bot class is also tailor made for running the API tests for freechat
    and  thus has a bunch of state which is must maintain
    '''
    def __init__(self, uid: int, secret: str):
        self.sesion: Session = Session()
        self.tests: list[Test] = []

        self.jwt: Optional[str] = None
        self.uid: int = uid
        self.secret: secret = secret

    def get(self, url: str, **kwargs):
        t = Test('GET', url, **kwargs)
        t.fire(self.session())
        self.tests.append(t)

    def post(self, url: str, **kwargs):
        t = Test('POST', url, **kwargs)
        t.fire(self.session())
        self.tests.append(t)

    def report(self):
        [print(i) for i in self.tests]


if __name__ == "__main__":
    # Simple passing tests which only serve as sanity checks

    bot = Bot()
    bot.post('http://localhost:8000/invite/create')
    bot.get('http://localhost:8000/channels/list')
    bot.report()

